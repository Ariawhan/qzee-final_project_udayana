#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<ctype.h>
#include<conio.h>

void cover();
void bantuan ();
void coverhitung();
int main()
{
    char pilihan;
    char namapemain[100];
    cover();
    system("cls");
    printf("\n\t\t --------------------------------------\n");
    printf("\t\t|           PROGRAM QUIZ GAME          |");
    printf("\n\t\t|    JADILAH SESEORANG YANG CERDAS     |");
    printf("\n\t\t --------------------------------------");
    printf("\n\t\t|           NAMA KAMU SIAPA ?          |");
    printf("\n\t\t --------------------------------------");
    printf("\n");
    printf("\n\t\t Masukan Nama Kamu :");
    scanf("%s", namapemain);
    rumah:
    strupr(namapemain);
    cover();
    printf("\t\t > KAMU ORANG PINTAR %s            ", namapemain);
    printf("\n\t\t --------------------------------------\n");
    printf("\n\t\t >> Silakan tekan M untuk mulai   <<");
    printf("\n\t\t >> Silakan tekan B untuk bantuan <<");
    printf("\n\t\t >> Silakan tekan K untuk keluar dari quiz <<\n");
    pilihan=toupper(getch());
    if (pilihan =='B')
	{
     char tekan2;
     bantuan1:
     system("cls");
     printf("\n\t\t --------------------------------------\n");
     printf("\t\t|          PROGRAM QUIZ GAME           |");
     printf("\n\t\t|              BANTUAN!!            |");
     printf("\n\t\t --------------------------------------");
     printf("\n\t\t >> Program ini terdiri atas 2 Quiz, Hitung Cepat dan Tebak Kata");
     printf("\n\t\t >> Hitung Cepat berisi 3 level dan setiap level ada 6 soal");
     printf("\n\t\t >  Jika Kamu menjawab salah kamu akan keluar dan kalah");
     printf("\n\t\t >  jika benar kamu akan mendapatkan skor setiap soal 10");
     printf("\n\t\t >> Tebak Kata berisi 10 Soal dan akan menebak satu kata");
     printf("\n\t\t >  kamu akan di berikan 3 kempatan atau 3 nyawa");
     printf("\n\t\t >  jika kamu salah kesempatan akan berkurang");
     printf("\n\t\t >> Jika kamu berhasil menyelesaikan semua kamu adalah smart\n");
     printf("\n\t\t >>> Silakan tekan B untuk Kembali <<<");
     tekan2=toupper(getch());
     if (tekan2=='B')
     {
        goto rumah;
     }
     else
     {
         goto bantuan1;
     }

	}
     else if (pilihan=='K')
	{
	    system("cls");
        exit(0);
	}
    else if(pilihan=='M')
    {
        home2 :
        cover();
        printf("\t\t > KAMU ORANG PINTAR %s            ", namapemain);
        printf("\n\t\t --------------------------------------\n");
        printf("\n\t\t >> Tekan Q Untuk Quiz Mengitung Cepat <<");
        printf("\n\t\t >> Tekan W Untuk Quiz Tebak Kata      <<");
        printf("\n\t\t >> Tekan S Untuk Kembali kesebelumnya <<");
        pilihan=toupper(getch());
        if (pilihan =='Q')
        {
     int i,a,b,c,d,e,f,h,k,g, jawaban1, jawab, jawab2, jawaban2, jawab3, jawaban3, score;
     char tekancepat, tekankata;
     hitungdepan:
     score=0;
     system("cls");
     printf("\n\t\t --------------------------------------\n");
     printf("\t\t|            QUIZ HITUNG CEPAT         |");
     printf("\n\t\t|              PERINGATAN!!            |");
     printf("\n\t\t --------------------------------------");
     printf("\n\t\t > Quiz  ini memiliki 3 level  ");
     printf("\n\t\t > Disetiap level memiliki tingkat kesulitan yang berbeda");
     printf("\n\t\t > Bila kamu behasil menjawab dengan benar akan mendapatkan score ");
     printf("\n\t\t > Setiap soal memiliki 10 score");
     printf("\n\t\t > Jika kamu menjawab salah maka quiz akan selesai");
     printf("\n\t\t > Kamu bisa melihat score pada akhir quiz\n");
     printf("\n\t\t >>> Silakan tekan Y untuk memulai <<<");
     printf("\n\t\t >>> Silakan tekan B untuk Kembali <<<");
     tekankata=toupper(getch());
     if (tekankata=='B')
     {
           goto home2;
     }
     else if (tekankata=='Y')
     {
     for(i=0; i<3; i++)
     {
        coverhitung();
        printf("\n\t\t  Level 1    |           |  score : %d        ", score);
        printf("\n\t\t --------------------------------------\n");
        a=rand() % 9;
        b=rand() % 9;
        jawaban1=a+b;
        printf("\n\tSoal :\n\t%d + %d\n",a, b);
        printf("\n\tJawaban Kamu :");
        scanf ("%d", &jawab);
        if (jawab == jawaban1)
        {
            score=score+10;
        }
        else
        {
            gagalcover1:
            system("cls");
            printf("\n\t\t --------------------------------------\n");
            printf("\t\t|            QUIZ HITUNG CEPAT         |");
            printf("\n\t\t|          JANGAN MENYERAH YA :)       |");
            printf("\n\t\t --------------------------------------");
            printf("\n\t\t|             SCORE KAMU: %d            |", score);
            printf("\n\t\t --------------------------------------\n");
            printf("\n\t\t >>> Silakan tekan M untuk Mengulang <<<");
            printf("\n\t\t >>> Silakan tekan B untuk Kembali   <<<");
            tekancepat=toupper(getch());
            if (tekancepat=='M')
            {
                 goto hitungdepan;
            }
            else if (tekancepat=='B')
            {
                  goto home2;
            }
            else
            {
                goto gagalcover1;
            }

        }
        coverhitung();
        printf("\n\t\t  Level 1    |           |  score : %d        ", score);
        printf("\n\t\t --------------------------------------\n");
        c=rand() % 12;
        d=rand() % 12;
        jawaban2=c*d;
        printf("\n\tSoal :\n\t%d x %d\n",c, d);
        printf("\n\tJawaban Kamu :");
        scanf ("%d", &jawab2);
        if (jawab2 == jawaban2)
        {
            score=score+10;
        }
        else
        {
            gagalcover2:
            system("cls");
            printf("\n\t\t --------------------------------------\n");
            printf("\t\t|            QUIZ HITUNG CEPAT         |");
            printf("\n\t\t|          JANGAN MENYERAH YA :)       |");
            printf("\n\t\t --------------------------------------");
            printf("\n\t\t|             SCORE KAMU: %d            |", score);
            printf("\n\t\t --------------------------------------\n");
            printf("\n\t\t >>> Silakan tekan M untuk Mengulang <<<");
            printf("\n\t\t >>> Silakan tekan B untuk Kembali   <<<");
            tekancepat=toupper(getch());
            if (tekancepat=='M')
            {
              goto hitungdepan;
            }
            else if (tekancepat=='B')
            {
               goto home2;
            }
            else
            {
            goto gagalcover2;
            }
    }
        coverhitung();
        printf("\n\t\t  Level 1    |           |  score : %d        ", score);
        printf("\n\t\t --------------------------------------\n");
        e=rand() % 15;
        f=rand() % 15;
        jawaban3=e-f;
        printf("\n\tSoal :\n\t%d - %d\n",e, f);
                printf("\n\tJawaban Kamu :");
                scanf ("%d", &jawab3);
                if (jawab3 == jawaban3)
                {
                    score=score+10;
                }
                else
                {
                    gagalcover3:
                    system("cls");
                    printf("\n\t\t --------------------------------------\n");
                    printf("\t\t|            QUIZ HITUNG CEPAT         |");
                    printf("\n\t\t|          JANGAN MENYERAH YA :)       |");
                    printf("\n\t\t --------------------------------------");
                    printf("\n\t\t|             SCORE KAMU: %d            |", score);
                    printf("\n\t\t --------------------------------------\n");
                    printf("\n\t\t >>> Silakan tekan M untuk Mengulang <<<");
                    printf("\n\t\t >>> Silakan tekan B untuk Kembali   <<<");
                    tekancepat=toupper(getch());
                    if (tekancepat=='M')
                    {
                             goto hitungdepan;
                    }
                    else if (tekancepat=='B')
                    {
                      goto home2;
                    }
                    else
                    {
                        goto gagalcover3;
                    }

                }
            }

            for(i=0; i<3; i++)
            {
                coverhitung();
                printf("\n\t\t  Level 2    |           |  score : %d        ", score);
                printf("\n\t\t --------------------------------------\n");
                a=rand() % 9;
                b=rand() % 9;
                jawaban1=a+b;
                printf("\n\tsoal :\n\t%d + %d\n",a, b);
                printf("\n\tJawaban Kamu :");
                scanf ("%d", &jawab);
                if (jawab == jawaban1)
                {
                    score=score+10;
                }
                else
                {
                gagalcover4:
                system("cls");
                printf("\n\t\t --------------------------------------\n");
                printf("\t\t|            QUIZ HITUNG CEPAT         |");
                printf("\n\t\t|          JANGAN MENYERAH YA :)       |");
                printf("\n\t\t --------------------------------------");
                printf("\n\t\t|             SCORE KAMU: %d            |", score);
                printf("\n\t\t --------------------------------------\n");
                printf("\n\t\t >>> Silakan tekan M untuk Mengulang <<<");
                printf("\n\t\t >>> Silakan tekan B untuk Kembali   <<<");
                tekancepat=toupper(getch());
                if (tekancepat=='M')
                {
                    goto hitungdepan;
                }
                else if (tekancepat=='B')
                {
                   goto home2;
                }
                else
                {
                    goto gagalcover4;
                }

                }
                coverhitung();
                printf("\n\t\t  Level 2    |           |  score : %d        ", score);
                printf("\n\t\t --------------------------------------\n");
                c=rand() % 12;
                d=rand() % 12;
                e=rand() % 12;
                jawaban2=c+(d*e);
                printf("\n\tsoal :\n\t%d + %d x %d\n",c, d, e);
                printf("\n\tJawaban Kamu :");
                scanf ("%d", &jawab2);
                if (jawab2 == jawaban2)
                {
                    score=score+10;
                }
                else
                {
                gagalcover5:
                system("cls");
                printf("\n\t\t --------------------------------------\n");
                printf("\t\t|            QUIZ HITUNG CEPAT         |");
                printf("\n\t\t|          JANGAN MENYERAH YA :)       |");
                printf("\n\t\t --------------------------------------");
                printf("\n\t\t|             SCORE KAMU: %d            |", score);
                printf("\n\t\t --------------------------------------\n");
                printf("\n\t\t >>> Silakan tekan M untuk Mengulang <<<");
                printf("\n\t\t >>> Silakan tekan B untuk Kembali   <<<");
                tekancepat=toupper(getch());
                if (tekancepat=='M')
                   {
                     goto hitungdepan;
                   }
                else if (tekancepat=='B')
                {
                      goto home2;
                }
                else
                {
                    goto gagalcover5;
                }

                }
                coverhitung();
                printf("\n\t\t  Level 2    |           |  score : %d        ", score);
                printf("\n\t\t --------------------------------------\n");
                f=rand() % 15;
                g=rand() % 15;
                h=rand() % 15;
                k=rand() % 15;
                jawaban3=f+(g*h)-k;
                printf("\n\tsoal :\n\t%d + %d x %d - %d\n",f, g, h,k);
                printf("\n\tJawaban Kamu :");
                scanf ("%d", &jawab3);
                if (jawab3 == jawaban3)
                {
                    score=score+10;
                }
                else
                {
                    gagalcover6:
                    system("cls");
                    printf("\n\t\t --------------------------------------\n");
                    printf("\t\t|            QUIZ HITUNG CEPAT         |");
                    printf("\n\t\t|          JANGAN MENYERAH YA :)       |");
                    printf("\n\t\t --------------------------------------");
                    printf("\n\t\t|             SCORE KAMU: %d            |", score);
                    printf("\n\t\t --------------------------------------\n");
                    printf("\n\t\t >>> Silakan tekan M untuk Mengulang <<<");
                    printf("\n\t\t >>> Silakan tekan B untuk Kembali   <<<");
                    tekancepat=toupper(getch());
                    if (tekancepat=='M')
                       {
                         goto  hitungdepan;
                       }
                    else if (tekancepat=='B')
                    {
                       goto home2;
                    }
                    else
                    {
                        goto gagalcover6;
                    }

                            }
                        }
        char soal[6][100]={"6 / 2 - 2 x 1 + 7",
                            "17 - 2 / 2 + 3 x 2",
                            "15 / 3 + 25 x 2 - 5",
                            "25 x 3 / 5 + 7 - 2",
                            "21 / 7 + 6 x 2 - 2",
                            "24 / 4 + 5  x 6 - 2"};
        char jawaban[6][100]={"8","22", "50", "20", "13", "34"};
            for (i=1; i<=6; i++)
            {
                char kata1[100];
                coverhitung();
                printf("\n\t\t  Level 3    |           |  score : %d        ", score);
                printf("\n\t\t --------------------------------------\n");
                printf("\tSoal :\n\t%s", soal[i-1]);
                printf("\n\n\tJawaban  Kamu :");
                scanf("%s", kata1);
                if(strcmp(kata1,jawaban[i-1])==0)
                {
                    score=score+10;
                }
                else
                {
                    gagalcover7:
                    system("cls");
                    printf("\n\t\t --------------------------------------\n");
                    printf("\t\t|            QUIZ HITUNG CEPAT         |");
                    printf("\n\t\t|          JANGAN MENYERAH YA :)       |");
                    printf("\n\t\t --------------------------------------");
                    printf("\n\t\t|             SCORE KAMU: %d            |", score);
                    printf("\n\t\t --------------------------------------\n");
                    printf("\n\t\t >>> Silakan tekan M untuk Mengulang <<<");
                    printf("\n\t\t >>> Silakan tekan B untuk Kembali   <<<");
                    tekancepat=toupper(getch());
                    if (tekancepat=='M')
                       {
                          goto hitungdepan;
                       }
                    else if (tekancepat=='B')
                    {
                        goto home2;
                    }
                    else
                    {
                        goto gagalcover7;
                    }

                }
            }
            char tekan3;
            menanghitungq:
            system("cls");
            printf("\n\t\t --------------------------------------\n");
            printf("\t\t|            QUIZ HITUNG CEPAT         |");
            printf("\n\t\t|           KAMU SMART SEKALI        |");
            printf("\n\t\t --------------------------------------");
            printf("\n\t\t              SCORE KAMU: %d            ", score);
            printf("\n\t\t --------------------------------------\n");
            printf("\n\t\t >>> Silakan tekan M untuk Mengulang <<<");
            printf("\n\t\t >>> Silakan tekan B untuk Kembali   <<<");
            tekan3=toupper(getch());
            if (tekan3=='M')
            {
               goto hitungdepan;
            }
            else if (tekan3=='B')
            {
               goto home2;
            }
            else
            {
                goto menanghitungq;
            }
     }
     else
     {
         goto hitungdepan;
     }
        }
        else if (pilihan=='W')
        {
        int i, nyawa, menang;
        char tekankata, tekan2;
        quiz22:
        menang=0;
        nyawa=3;
        system("cls");
        printf("\n\t\t --------------------------------------\n");
        printf("\t\t|            QUIZ TEBAK KATA           |");
        printf("\n\t\t|              PERINGATAN!!            |");
        printf("\n\t\t --------------------------------------");
        printf("\n\t\t > Jumlah soal di quiz ini ada 10 soal ");
        printf("\n\t\t > Kamu cukup menebak 1 kata dari soal tersebut ");
        printf("\n\t\t > Kamu Akan Mendapatkan 3 Kesempatan ");
        printf("\n\t\t > Jika kesempatan habis maka kamu akan kalah\n");
        printf("\n\t\t >>> Silakan tekan Y untuk memulai <<<");
        printf("\n\t\t >>> Silakan tekan B untuk Kembali <<<");
        tekankata=toupper(getch());
        if (tekankata=='B')
        {
           goto home2;
        }
        else if (tekankata=='Y')
        {
         char soal[10][100]={"Hewan yang bisa hidup di dua tempat dan namanya bisa dibolak balik",
                            "Suatu hal yang memiliki cabang tetapi tidak memiliki daun dan juga buah",
                            "Sebuah bangunan yang memiliki banyak cerita",
                            "Sesuatu yang dapat naik dan juga turun tanpa melakukan pergerakan",
                            "Kau selalu menjawabku walau aku tak pernah menanyakan sesuatu",
                            "Beri dia makan maka dia akan hidup, beri dia minum maka dia akan mati",
                            "Sesuatu yang bisa terbang tetapi tidak memiliki sayap",
                            "Suatu benda yang dapat mengelilingi dunia dan letaknya selalu di pojok",
                            "Sesuatu yang harus dirusak sebelum digunakan",
                            "Dia tinggi saat masih muda dan pendek saat mengalami penuaan"};
         char jawaban[10][100]={"katak","bank", "perpustakaan", "suhu", "telephone", "api", "waktu", "perangko", "telor","lilin"};
         for (i=1; i<=10; i++)
         {

            if (nyawa > 0)
            {
                system("cls");
                printf("\n\t\t --------------------------------------\n");
                printf("\t\t|            QUIZ TEBAK KATA           |");
                printf("\n\t\t|              SEMANGAT!!              |");
                printf("\n\t\t --------------------------------------");
                printf("\n\t\t| Kesempatan : %d     | Menang : %d      |", nyawa, menang);
                printf("\n\t\t --------------------------------------");
                printf("\n\n");
                char kata1[100];
                printf("\tSoal :\n\t%s", soal[i-1]);
                printf("\n\n\tTebakan Kamu :");
                scanf("%s", kata1);
                strlwr(kata1);
                if(strcmp(kata1,jawaban[i-1])==0)
                {
                    menang=menang+1;
                }
                else
                {
                    nyawa=nyawa-1;
                }
                }

                else
                {
                    char tekan8;
                    gagal:
                    system("cls");
                    printf("\n\t\t --------------------------------------\n");
                    printf("\t\t|            QUIZ TEBAK KATA           |");
                    printf("\n\t\t|          JANGAN MENYERAH YA :)       |");
                    printf("\n\t\t --------------------------------------");
                    printf("\n\t\t|         Kesempatan Kamu Habiss       |");
                    printf("\n\t\t --------------------------------------\n");
                    printf("\n\t\t >>> Silakan tekan M untuk Mengulang <<<");
                    printf("\n\t\t >>> Silakan tekan B untuk Kembali   <<<");
                    tekan8=toupper(getch());
                    if (tekan8=='M')
                    {
                       goto quiz22;
                    }
                    else if (tekan8=='B')
                    {
                      goto home2;
                    }
                    else
                    {
                        goto gagal;
                    }
                }
            }
            char tekan9;
            menangtebak:
            system("cls");
            printf("\n\t\t --------------------------------------\n");
            printf("\t\t|            QUIZ TEBAK KATA           |");
            printf("\n\t\t|          KAMU SMART SEKALI      |");
            printf("\n\t\t --------------------------------------");
            printf("\n\t\t >>> Silakan tekan M untuk Mengulang <<<");
            printf("\n\t\t >>> Silakan tekan B untuk Kembali   <<<");
            tekan9=toupper(getch());
            if (tekan9=='M')
            {
                goto quiz22;
            }
            else if (tekan9=='B')
            {
                goto home2;
            }
            else
            {
                goto menangtebak;
            }
        }
        else
        {
            goto quiz22;
        }
        }
        else if(pilihan=='S')
        {
            goto rumah;
        }
        else
        {
            goto home2;
        }
    }
    else
    {
        goto rumah;
    }
}

void cover()
    {
        system("cls");
        printf("\n\t\t --------------------------------------\n");
        printf("\t\t|          PROGRAM QUIZ GAME           |");
        printf("\n\t\t|              WELCOME                 |");
        printf("\n\t\t --------------------------------------");
        printf("\n");
    }
void coverhitung ()
{

        system("cls");
        printf("\n\t\t --------------------------------------\n");
        printf("\t\t|            QUIZ HITUNG CEPAT         |");
        printf("\n\t\t|              SEMANGAT!!              |");
        printf("\n\t\t --------------------------------------");

}
